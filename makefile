#------------------------------------------------------------------------------
#
# SuperNova APPLICATION - BL - MAKEFILE
# By FIST -  Kresimir Mandic 27.03.1999
#
#------------------------------------------------------------------------------

# Compiler options
#------------------------------------------------------------------------------
## CC          = start /w c:\snova\sncd\nova
CC          = start /w c:\vscd\bin\nova


CFLAGS      = -w -ereadfgl
TD			= X:\bl\ 
YTD			= Y:\NewVer.RAR\bl\ 
COMD        = .\lib\blcommon.fle 

BLFILES    = $(TD)bl.fle $(TD)bl_flds.fle  $(TD)bl_blizv.fle $(TD)bl_br_pn.fle \
      $(TD)bl_opom.fle   $(TD)bl_kart.fle  $(TD)bl_kartv.fle $(TD)bl_blag.fle  \
      $(TD)bl_lispn.fle  $(TD)bl_rad_v.fle $(TD)bl_obrcl.fle $(TD)bl_pdoha.fle \
      $(TD)bl_pnal.fle   $(TD)bl_pnizv.fle $(TD)bl_pnobr.fle $(TD)bl_ponii.fle \
      $(TD)bl_prsrd.fle  $(TD)bl_rispl.fle $(TD)bl_rkart.fle $(TD)bl_rkpn.fle  \
      $(TD)bl_rpnal.fle $(TD)bl_rponi.fle $(TD)bl_rsbl.fle  \
      $(TD)bl_slipn.fle  $(TD)bl_sljed.fle $(TD)bl_stpar.fle $(TD)bl_stpn2.fle \
      $(TD)bl_stvbl.fle  $(TD)bl_stvpn.fle $(TD)bl_tecaj.fle $(TD)bl_temgk.fle \
      $(TD)bl_trsk.fle   $(TD)bl_vrpn.fle  $(TD)bl_vrstr.fle $(TD)getovi.fle   \
      $(TD)updtec.fle    $(TD)l_opom.fle   $(TD)bslova.fle   $(TD)bl_raobr.fle \
      $(TD)bl_virmd.fle  $(TD)bl_virm.fle  $(TD)bl_lvirm.fle $(TD)bl_dd.fle    \
      $(TD)l_opom_v.fle $(TD)bl_ngod.fle   $(TD)bl_azsbl.fle $(TD)bl_start.fle \
      $(TD)pn_ngod.fle   $(TD)bldatisp.fle $(TD)bldbutil.fle $(TD)bl_robrv.fle \
      $(TD)bl_a_dd.fle  $(TD)bl_astbl.fle  $(TD)bl_idx.fle   $(TD)rba_eduk.fle \
      $(TD)lrba_eduk.fle $(TD)l_trosk.fle  $(TD)hpapn_sumd.fle $(TD)l_sumtros.fle \
      $(TD)bl_rev.fle $(TD)blizvj_jop.fle  $(TD)pnizvj_jop.fle $(TD)pnizvj_rek.fle \
      $(TD)load_hpa_bl.fle $(TD)hpa_blag.fle 
       
COMMON     = bl_blag.ls bl_dd.ls bl_ddx.ls bl_flds.ls bl_trsk.ls bl_vrpn.ls \
             bl_vrstr.ls bslova.ls   


all     :  $(BLFILES) 


## COMMON

bl_blag.ls:  bl_blag.lgc
    $(CC) $(CFLAGS) appl=bl_blag dotfle=$(COMD)
    @echo " ">bl_blag.ls

bl_dd.ls:  bl_dd.lgc
    $(CC) $(CFLAGS) appl=bl_dd dotfle=.\lib\bl_dd.fle 
    @echo " ">bl_dd.ls

bl_ddx.ls:  bl_ddx.lgc
    $(CC) $(CFLAGS) appl=bl_ddx dotfle=.\lib\bl_dd.fle 
    @echo " ">bl_ddx.ls

bl_flds.ls:  bl_flds.lgc
    $(CC) $(CFLAGS) appl=bl_flds dotfle=.\lib\bl_dd.fle 
    @echo " ">bl_flds.ls

bl_trsk.ls:  bl_trsk.lgc
    $(CC) $(CFLAGS) appl=bl_trsk dotfle=$(COMD)
    @echo " ">bl_trsk.ls

bl_vrpn.ls:  bl_vrpn.lgc
    $(CC) $(CFLAGS) appl=bl_vrpn dotfle=$(COMD)
    @echo " ">bl_vrpn.ls

bl_vrstr.ls:  bl_vrstr.lgc
    $(CC) $(CFLAGS) appl=bl_vrstr dotfle=$(COMD)
    @echo " ">bl_vrstr.ls

bslova.ls:  bslova.lgc
    $(CC) $(CFLAGS) appl=bslova dotfle=$(COMD)
    @echo " ">bslova.ls



#####################3
$(TD)bl_dd.fle:  bl_dd.lgc
    $(CC) $(CFLAGS) appl=bl_dd dotfle=$(TD)bl_dd.fle
    $(CC) -w genddx ddfile=bl_dd
    $(CC) $(CFLAGS) appl=bl_ddx dotfle=$(TD)bl_dd.fle
    
$(TD)bl.fle:  bl.lgc
    $(CC) $(CFLAGS) appl=bl dotfle=$(TD)bl.fle

$(TD)bl_flds.fle:  bl_flds.lgc
    $(CC) $(CFLAGS) appl=bl_flds dotfle=$(TD)bl_flds.fle

$(TD)bl_aszbl.fle:  bl_aszbl.lgc
    $(CC) $(CFLAGS) appl=bl_aszbl dotfle=$(TD)bl_aszbl.fle

$(TD)bl_blag.fle:  bl_blag.lgc
    $(CC) $(CFLAGS) appl=bl_blag dotfle=$(TD)bl_blag.fle

$(TD)bl_blizv.fle:  bl_blizv.lgc
    $(CC) $(CFLAGS) appl=bl_blizv dotfle=$(TD)bl_blizv.fle

$(TD)bl_br_pn.fle:  bl_br_pn.lgc
    $(CC) $(CFLAGS) appl=bl_br_pn dotfle=$(TD)bl_br_pn.fle

$(TD)bl_kart.fle:  bl_kart.lgc
    $(CC) $(CFLAGS) appl=bl_kart dotfle=$(TD)bl_kart.fle

$(TD)bl_kartv.fle:  bl_kartv.lgc
    $(CC) $(CFLAGS) appl=bl_kartv dotfle=$(TD)bl_kartv.fle

$(TD)bl_lispn.fle:  bl_lispn.lgc
    $(CC) $(CFLAGS) appl=bl_lispn dotfle=$(TD)bl_lispn.fle
    
$(TD)bl_obrcl.fle:  bl_obrcl.lgc
    $(CC) $(CFLAGS) appl=bl_obrcl dotfle=$(TD)bl_obrcl.fle
    
$(TD)bl_pdoha.fle:  bl_pdoha.lgc
    $(CC) $(CFLAGS) appl=bl_pdoha dotfle=$(TD)bl_pdoha.fle
    
$(TD)bl_pnal.fle:  bl_pnal.lgc
    $(CC) $(CFLAGS) appl=bl_pnal dotfle=$(TD)bl_pnal.fle
    
$(TD)bl_pnizv.fle:  bl_pnizv.lgc
    $(CC) $(CFLAGS) appl=bl_pnizv dotfle=$(TD)bl_pnizv.fle
    
$(TD)bl_pnobr.fle:  bl_pnobr.lgc
    $(CC) $(CFLAGS) appl=bl_pnobr dotfle=$(TD)bl_pnobr.fle
    
$(TD)bl_ponii.fle:  bl_ponii.lgc
    $(CC) $(CFLAGS) appl=bl_ponii dotfle=$(TD)bl_ponii.fle
    
$(TD)bl_prsrd.fle:  bl_prsrd.lgc
    $(CC) $(CFLAGS) appl=bl_prsrd dotfle=$(TD)bl_prsrd.fle
    
$(TD)bl_rispl.fle:  bl_rispl.lgc
    $(CC) $(CFLAGS) appl=bl_rispl dotfle=$(TD)bl_rispl.fle
    
$(TD)bl_rkart.fle:  bl_rkart.lgc
    $(CC) $(CFLAGS) appl=bl_rkart dotfle=$(TD)bl_rkart.fle
    
$(TD)bl_rkpn.fle:  bl_rkpn.lgc
    $(CC) $(CFLAGS) appl=bl_rkpn dotfle=$(TD)bl_rkpn.fle
    
$(TD)bl_robrv.fle:  bl_robrv.lgc
    $(CC) $(CFLAGS) appl=bl_robrv dotfle=$(TD)bl_robrv.fle
    
$(TD)bl_rpnal.fle:  bl_rpnal.lgc
    $(CC) $(CFLAGS) appl=bl_rpnal dotfle=$(TD)bl_rpnal.fle
    
$(TD)bl_rponi.fle:  bl_rponi.lgc
    $(CC) $(CFLAGS) appl=bl_rponi dotfle=$(TD)bl_rponi.fle
    
$(TD)bl_rsbl.fle:  bl_rsbl.lgc
    $(CC) $(CFLAGS) appl=bl_rsbl dotfle=$(TD)bl_rsbl.fle
    
$(TD)bl_slipn.fle:  bl_slipn.lgc
    $(CC) $(CFLAGS) appl=bl_slipn dotfle=$(TD)bl_slipn.fle
    
$(TD)bl_sljed.fle:  bl_sljed.lgc
    $(CC) $(CFLAGS) appl=bl_sljed dotfle=$(TD)bl_sljed.fle
    
$(TD)bl_stpar.fle:  bl_stpar.lgc
    $(CC) $(CFLAGS) appl=bl_stpar dotfle=$(TD)bl_stpar.fle
    
$(TD)bl_stpn2.fle:  bl_stpn2.lgc
    $(CC) $(CFLAGS) appl=bl_stpn2 dotfle=$(TD)bl_stpn2.fle
    
$(TD)bl_stvbl.fle:  bl_stvbl.lgc
    $(CC) $(CFLAGS) appl=bl_stvbl dotfle=$(TD)bl_stvbl.fle
    
$(TD)bl_stvpn.fle:  bl_stvpn.lgc
    $(CC) $(CFLAGS) appl=bl_stvpn dotfle=$(TD)bl_stvpn.fle
    
$(TD)bl_tecaj.fle:  bl_tecaj.lgc
    $(CC) $(CFLAGS) appl=bl_tecaj dotfle=$(TD)bl_tecaj.fle
    
$(TD)bl_temgk.fle:  bl_temgk.lgc
    $(CC) $(CFLAGS) appl=bl_temgk dotfle=$(TD)bl_temgk.fle
    
$(TD)bl_trsk.fle:  bl_trsk.lgc
    $(CC) $(CFLAGS) appl=bl_trsk dotfle=$(TD)bl_trsk.fle
    
$(TD)bl_vrpn.fle:  bl_vrpn.lgc
    $(CC) $(CFLAGS) appl=bl_vrpn dotfle=$(TD)bl_vrpn.fle
    
$(TD)bl_vrstr.fle:  bl_vrstr.lgc
    $(CC) $(CFLAGS) appl=bl_vrstr dotfle=$(TD)bl_vrstr.fle
    
$(TD)getovi.fle:  getovi.lgc
    $(CC) $(CFLAGS) appl=getovi dotfle=$(TD)getovi.fle
    
$(TD)updtec.fle:  updtec.lgc
    $(CC) $(CFLAGS) appl=updtec dotfle=$(TD)updtec.fle
    
$(TD)l_opom.fle:  l_opom.lgc
    $(CC) $(CFLAGS) appl=l_opom dotfle=$(TD)l_opom.fle
    
$(TD)bl_opom.fle:  bl_opom.lgc
    $(CC) $(CFLAGS) appl=bl_opom dotfle=$(TD)bl_opom.fle
    
$(TD)l_opom_v.fle:  l_opom_v.lgc
    $(CC) $(CFLAGS) appl=l_opom_v dotfle=$(TD)l_opom_v.fle
    
$(TD)bslova.fle:  bslova.lgc
    $(CC) $(CFLAGS) appl=bslova dotfle=$(TD)bslova.fle
    
$(TD)bl_raobr.fle:  bl_raobr.lgc
    $(CC) $(CFLAGS) appl=bl_raobr dotfle=$(TD)bl_raobr.fle
    
$(TD)bl_virmd.fle:  bl_virmd.lgc
    $(CC) $(CFLAGS) appl=bl_virmd dotfle=$(TD)bl_virmd.fle
    
$(TD)bl_virm.fle:  bl_virm.lgc
    $(CC) $(CFLAGS) appl=bl_virm dotfle=$(TD)bl_virm.fle
    
$(TD)bl_lvirm.fle:  bl_lvirm.lgc
    $(CC) $(CFLAGS) appl=bl_lvirm dotfle=$(TD)bl_lvirm.fle
    
$(TD)bl_rad_v.fle:  bl_rad_v.lgc
    $(CC) $(CFLAGS) appl=bl_rad_v dotfle=$(TD)bl_rad_v.fle
    
$(TD)bl_ngod.fle:  bl_ngod.lgc
    $(CC) $(CFLAGS) appl=bl_ngod dotfle=$(TD)bl_ngod.fle
    
$(TD)pn_ngod.fle:  pn_ngod.lgc
    $(CC) $(CFLAGS) appl=pn_ngod dotfle=$(TD)pn_ngod.fle
    
$(TD)bldatisp.fle:  bldatisp.lgc
    $(CC) $(CFLAGS) appl=bldatisp dotfle=$(TD)bldatisp.fle
    
$(TD)bldbutil.fle:  bldbutil.lgc
    $(CC) $(CFLAGS) appl=bldbutil dotfle=$(TD)bldbutil.fle

$(TD)bl_azsbl.fle:  bl_azsbl.lgc
    $(CC) $(CFLAGS) appl=bl_azsbl dotfle=$(TD)bl_azsbl.fle

$(TD)bl_a_dd.fle:  bl_a_dd.lgc
    $(CC) $(CFLAGS) appl=bl_a_dd dotfle=$(TD)bl_a_dd.fle

$(TD)bl_astbl.fle:  bl_astbl.lgc
    $(CC) $(CFLAGS) appl=bl_astbl dotfle=$(TD)bl_astbl.fle

$(TD)bl_idx.fle:  bl_idx.lgc
    $(CC) $(CFLAGS) appl=bl_idx dotfle=$(TD)bl_idx.fle

$(TD)rba_eduk.fle:  rba_eduk.lgc
    $(CC) $(CFLAGS) appl=rba_eduk dotfle=$(TD)rba_eduk.fle

$(TD)lrba_eduk.fle:  lrba_eduk.lgc
    $(CC) $(CFLAGS) appl=lrba_eduk dotfle=$(TD)lrba_eduk.fle

$(TD)l_trosk.fle:  l_trosk.lgc
    $(CC) $(CFLAGS) appl=l_trosk dotfle=$(TD)l_trosk.fle

$(TD)hpapn_sumd.fle:  hpapn_sumd.lgc
    $(CC) $(CFLAGS) appl=hpapn_sumd dotfle=$(TD)hpapn_sumd.fle

$(TD)l_sumtros.fle:  l_sumtros.lgc
    $(CC) $(CFLAGS) appl=l_sumtros dotfle=$(TD)l_sumtros.fle

$(TD)bl_start.fle:  bl_start.lgc
    $(CC) $(CFLAGS) appl=bl_start dotfle=$(TD)bl_start.fle

$(TD)bl_rev.fle:  bl_rev.lgc
    $(CC) $(CFLAGS) appl=bl_rev dotfle=$(TD)bl_rev.fle

$(TD)blizvj_jop.fle:  blizvj_jop.lgc
    $(CC) $(CFLAGS) appl=blizvj_jop dotfle=$(TD)blizvj_jop.fle

$(TD)pnizvj_jop.fle:  pnizvj_jop.lgc
    $(CC) $(CFLAGS) appl=pnizvj_jop dotfle=$(TD)pnizvj_jop.fle

$(TD)pnizvj_rek.fle:  pnizvj_rek.lgc
    $(CC) $(CFLAGS) appl=pnizvj_rek dotfle=$(TD)pnizvj_rek.fle
    
$(TD)load_hpa_bl.fle:  load_hpa_bl.lgc
    $(CC) $(CFLAGS) appl=load_hpa_bl dotfle=$(TD)load_hpa_bl.fle

$(TD)hpa_blag.fle:  hpa_blag.lgc
    $(CC) $(CFLAGS) appl=hpa_blag dotfle=$(TD)hpa_blag.fle

